/**
 * 
 */
package com.id.kpi.enums;

/**
 * @author aulia
 *
 */
public enum QuestionDetailEnum {

	HORIZONTAL, VERTICAL;

}
