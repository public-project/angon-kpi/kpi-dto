/**
 * 
 */
package com.id.kpi.enums;

/**
 * @author aulia
 *
 */
public enum StatusEnum {

	ACTIVE, INACTIVE;

}
