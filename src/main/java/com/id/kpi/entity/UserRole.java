/**
 * 
 */
package com.id.kpi.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.id.kpi.base.BaseEntity;
import com.id.kpi.enums.StatusEnum;

/**
 * @author aulia
 *
 */
@Entity
@Table
public class UserRole extends BaseEntity {

	private static final long serialVersionUID = 8807564051304521932L;

	@Column
	private String roleName;

	@Column
	private Boolean isCreate;

	@Column
	private Boolean isDelete;

	@Column
	private Boolean isUpdate;

	@Column
	private Boolean isView;

	@Column
	@Enumerated(EnumType.STRING)
	private StatusEnum status;

	/**
	 * Default constructor
	 */
	public UserRole() {
		super();
	}

	/**
	 * @param roleName
	 * @param isCreate
	 * @param isDelete
	 * @param isUpdate
	 * @param isView
	 * @param status
	 */
	public UserRole(String roleName, Boolean isCreate, Boolean isDelete, Boolean isUpdate, Boolean isView,
			StatusEnum status) {
		this.roleName = roleName;
		this.isCreate = isCreate;
		this.isDelete = isDelete;
		this.isUpdate = isUpdate;
		this.isView = isView;
		this.status = status;
	}

	/**
	 * @return the roleName
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * @param roleName
	 *            the roleName to set
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * @return the isCreate
	 */
	public Boolean getIsCreate() {
		return isCreate;
	}

	/**
	 * @param isCreate
	 *            the isCreate to set
	 */
	public void setIsCreate(Boolean isCreate) {
		this.isCreate = isCreate;
	}

	/**
	 * @return the isDelete
	 */
	public Boolean getIsDelete() {
		return isDelete;
	}

	/**
	 * @param isDelete
	 *            the isDelete to set
	 */
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	/**
	 * @return the isUpdate
	 */
	public Boolean getIsUpdate() {
		return isUpdate;
	}

	/**
	 * @param isUpdate
	 *            the isUpdate to set
	 */
	public void setIsUpdate(Boolean isUpdate) {
		this.isUpdate = isUpdate;
	}

	/**
	 * @return the isView
	 */
	public Boolean getIsView() {
		return isView;
	}

	/**
	 * @param isView
	 *            the isView to set
	 */
	public void setIsView(Boolean isView) {
		this.isView = isView;
	}

	/**
	 * @return the status
	 */
	public StatusEnum getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	@Override
	@PrePersist
	public void prePersist() {
		setCreatedDate(new Date());

		if (getIsCreate() == null) {
			setIsCreate(Boolean.FALSE);
		}
		if (getIsDelete() == null) {
			setIsDelete(Boolean.FALSE);
		}
		if (getIsUpdate() == null) {
			setIsUpdate(Boolean.FALSE);
		}
		if (getIsView() == null) {
			setIsView(Boolean.FALSE);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((isCreate == null) ? 0 : isCreate.hashCode());
		result = prime * result + ((isDelete == null) ? 0 : isDelete.hashCode());
		result = prime * result + ((isUpdate == null) ? 0 : isUpdate.hashCode());
		result = prime * result + ((isView == null) ? 0 : isView.hashCode());
		result = prime * result + ((roleName == null) ? 0 : roleName.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof UserRole))
			return false;
		UserRole other = (UserRole) obj;
		if (isCreate == null) {
			if (other.isCreate != null)
				return false;
		} else if (!isCreate.equals(other.isCreate))
			return false;
		if (isDelete == null) {
			if (other.isDelete != null)
				return false;
		} else if (!isDelete.equals(other.isDelete))
			return false;
		if (isUpdate == null) {
			if (other.isUpdate != null)
				return false;
		} else if (!isUpdate.equals(other.isUpdate))
			return false;
		if (isView == null) {
			if (other.isView != null)
				return false;
		} else if (!isView.equals(other.isView))
			return false;
		if (roleName == null) {
			if (other.roleName != null)
				return false;
		} else if (!roleName.equals(other.roleName))
			return false;
		if (status != other.status)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserRole [roleName=" + roleName + ", isCreate=" + isCreate + ", isDelete=" + isDelete + ", isUpdate="
				+ isUpdate + ", isView=" + isView + ", status=" + status + "]";
	}

}
