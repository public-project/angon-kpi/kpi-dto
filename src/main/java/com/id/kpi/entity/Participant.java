/**
 * 
 */
package com.id.kpi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.id.kpi.base.BaseEntity;
import com.id.kpi.enums.StatusEnum;

/**
 * @author aulia
 *
 */
@Entity
@Table
public class Participant extends BaseEntity {

	private static final long serialVersionUID = 1955445248266040323L;

	@Column
	private String participantCode;

	/**
	 * Need to confirm this. Will it be save in different table or use same table. 
	 */
	@Column
	private String participantClass;

	@Column
	private String participantFullName;

	@Column
	@Enumerated(EnumType.STRING)
	private StatusEnum status;

	/**
	 * Default constructor
	 */
	public Participant() {
		super();
	}

	/**
	 * @param participantCode
	 * @param participantClass
	 * @param participantFullName
	 * @param status
	 */
	public Participant(String participantCode, String participantClass, String participantFullName, StatusEnum status) {
		this.participantCode = participantCode;
		this.participantClass = participantClass;
		this.participantFullName = participantFullName;
		this.status = status;
	}

	/**
	 * @return the participantCode
	 */
	public String getParticipantCode() {
		return participantCode;
	}

	/**
	 * @param participantCode
	 *            the participantCode to set
	 */
	public void setParticipantCode(String participantCode) {
		this.participantCode = participantCode;
	}

	/**
	 * @return the participantClass
	 */
	public String getParticipantClass() {
		return participantClass;
	}

	/**
	 * @param participantClass
	 *            the participantClass to set
	 */
	public void setParticipantClass(String participantClass) {
		this.participantClass = participantClass;
	}

	/**
	 * @return the participantFullName
	 */
	public String getParticipantFullName() {
		return participantFullName;
	}

	/**
	 * @param participantFullName
	 *            the participantFullName to set
	 */
	public void setParticipantFullName(String participantFullName) {
		this.participantFullName = participantFullName;
	}

	/**
	 * @return the status
	 */
	public StatusEnum getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((participantClass == null) ? 0 : participantClass.hashCode());
		result = prime * result + ((participantCode == null) ? 0 : participantCode.hashCode());
		result = prime * result + ((participantFullName == null) ? 0 : participantFullName.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Participant))
			return false;
		Participant other = (Participant) obj;
		if (participantClass == null) {
			if (other.participantClass != null)
				return false;
		} else if (!participantClass.equals(other.participantClass))
			return false;
		if (participantCode == null) {
			if (other.participantCode != null)
				return false;
		} else if (!participantCode.equals(other.participantCode))
			return false;
		if (participantFullName == null) {
			if (other.participantFullName != null)
				return false;
		} else if (!participantFullName.equals(other.participantFullName))
			return false;
		if (status != other.status)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Participant [participantCode=" + participantCode + ", participantClass=" + participantClass
				+ ", participantFullName=" + participantFullName + ", status=" + status + "]";
	}

}
