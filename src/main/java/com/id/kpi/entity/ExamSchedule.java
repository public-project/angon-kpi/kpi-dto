/**
 * 
 */
package com.id.kpi.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.id.kpi.base.BaseEntity;

/**
 * @author aulia
 *
 */
@Entity
@Table
public class ExamSchedule extends BaseEntity {

	private static final long serialVersionUID = -7948785748237899935L;

	@Temporal(TemporalType.DATE)
	@Column
	private Date startExamDate;

	@Temporal(TemporalType.TIME)
	@Column
	private Date startExamHour;

	@Column
	private String examClass;

	@Column
	private Boolean isReducedDuration;

	@Column
	private QuestionHeader questionHeader;

	/**
	 * Default constructor
	 */
	public ExamSchedule() {
		super();
	}

	/**
	 * @param startExamDate
	 * @param startExamHour
	 * @param examClass
	 * @param isReducedDuration
	 * @param questionHeader
	 */
	public ExamSchedule(Date startExamDate, Date startExamHour, String examClass, Boolean isReducedDuration,
			QuestionHeader questionHeader) {
		this.startExamDate = startExamDate;
		this.startExamHour = startExamHour;
		this.examClass = examClass;
		this.isReducedDuration = isReducedDuration;
		this.questionHeader = questionHeader;
	}

	/**
	 * @return the startExamDate
	 */
	public Date getStartExamDate() {
		return startExamDate;
	}

	/**
	 * @param startExamDate
	 *            the startExamDate to set
	 */
	public void setStartExamDate(Date startExamDate) {
		this.startExamDate = startExamDate;
	}

	/**
	 * @return the startExamHour
	 */
	public Date getStartExamHour() {
		return startExamHour;
	}

	/**
	 * @param startExamHour
	 *            the startExamHour to set
	 */
	public void setStartExamHour(Date startExamHour) {
		this.startExamHour = startExamHour;
	}

	/**
	 * @return the examClass
	 */
	public String getExamClass() {
		return examClass;
	}

	/**
	 * @param examClass
	 *            the examClass to set
	 */
	public void setExamClass(String examClass) {
		this.examClass = examClass;
	}

	/**
	 * @return the isReducedDuration
	 */
	public Boolean getIsReducedDuration() {
		return isReducedDuration;
	}

	/**
	 * @param isReducedDuration
	 *            the isReducedDuration to set
	 */
	public void setIsReducedDuration(Boolean isReducedDuration) {
		this.isReducedDuration = isReducedDuration;
	}

	/**
	 * @return the questionHeader
	 */
	public QuestionHeader getQuestionHeader() {
		return questionHeader;
	}

	/**
	 * @param questionHeader
	 *            the questionHeader to set
	 */
	public void setQuestionHeader(QuestionHeader questionHeader) {
		this.questionHeader = questionHeader;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((examClass == null) ? 0 : examClass.hashCode());
		result = prime * result + ((isReducedDuration == null) ? 0 : isReducedDuration.hashCode());
		result = prime * result + ((questionHeader == null) ? 0 : questionHeader.hashCode());
		result = prime * result + ((startExamDate == null) ? 0 : startExamDate.hashCode());
		result = prime * result + ((startExamHour == null) ? 0 : startExamHour.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof ExamSchedule))
			return false;
		ExamSchedule other = (ExamSchedule) obj;
		if (examClass == null) {
			if (other.examClass != null)
				return false;
		} else if (!examClass.equals(other.examClass))
			return false;
		if (isReducedDuration == null) {
			if (other.isReducedDuration != null)
				return false;
		} else if (!isReducedDuration.equals(other.isReducedDuration))
			return false;
		if (questionHeader == null) {
			if (other.questionHeader != null)
				return false;
		} else if (!questionHeader.equals(other.questionHeader))
			return false;
		if (startExamDate == null) {
			if (other.startExamDate != null)
				return false;
		} else if (!startExamDate.equals(other.startExamDate))
			return false;
		if (startExamHour == null) {
			if (other.startExamHour != null)
				return false;
		} else if (!startExamHour.equals(other.startExamHour))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ExamSchedule [startExamDate=" + startExamDate + ", startExamHour=" + startExamHour + ", examClass="
				+ examClass + ", isReducedDuration=" + isReducedDuration + ", questionHeader=" + questionHeader + "]";
	}

}
