/**
 * 
 */
package com.id.kpi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.id.kpi.base.BaseEntity;

/**
 * @author aulia
 *
 */
@Entity
public class ParticipantClass extends BaseEntity {

	private static final long serialVersionUID = -2880962934838809442L;

	@Column
	private String className;

	/**
	 * Default constructor
	 */
	public ParticipantClass() {
		super();
	}

	/**
	 * @param className
	 */
	public ParticipantClass(String className) {
		this.className = className;
	}

	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className
	 *            the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((className == null) ? 0 : className.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof ParticipantClass))
			return false;
		ParticipantClass other = (ParticipantClass) obj;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ParticipantClass [className=" + className + "]";
	}

}
