/**
 * 
 */
package com.id.kpi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.id.kpi.base.BaseEntity;

/**
 * @author aulia
 *
 */
@Entity
@Table
public class QuestionHeader extends BaseEntity {

	private static final long serialVersionUID = -947615311347847002L;

	@Column
	private String questionCode;

	@Column
	private String questionTitle;

	@Column
	private Long duration;

	/**
	 * Don't know what is this field used to. Need to find out more, will talk to
	 * Happy K.
	 */
	@Column
	private Long kkm;

	@Column
	private Boolean isRandomQuestion;

	@Column
	private Boolean isRandomAnswer;

	/**
	 * Default constructor
	 */
	public QuestionHeader() {
		super();
	}

	/**
	 * @param questionCode
	 * @param questionTitle
	 * @param duration
	 * @param kkm
	 * @param isRandomQuestion
	 * @param isRandomAnswer
	 */
	public QuestionHeader(String questionCode, String questionTitle, Long duration, Long kkm, Boolean isRandomQuestion,
			Boolean isRandomAnswer) {
		this.questionCode = questionCode;
		this.questionTitle = questionTitle;
		this.duration = duration;
		this.kkm = kkm;
		this.isRandomQuestion = isRandomQuestion;
		this.isRandomAnswer = isRandomAnswer;
	}

	/**
	 * @return the questionCode
	 */
	public String getQuestionCode() {
		return questionCode;
	}

	/**
	 * @param questionCode
	 *            the questionCode to set
	 */
	public void setQuestionCode(String questionCode) {
		this.questionCode = questionCode;
	}

	/**
	 * @return the questionTitle
	 */
	public String getQuestionTitle() {
		return questionTitle;
	}

	/**
	 * @param questionTitle
	 *            the questionTitle to set
	 */
	public void setQuestionTitle(String questionTitle) {
		this.questionTitle = questionTitle;
	}

	/**
	 * @return the duration
	 */
	public Long getDuration() {
		return duration;
	}

	/**
	 * @param duration
	 *            the duration to set
	 */
	public void setDuration(Long duration) {
		this.duration = duration;
	}

	/**
	 * @return the kkm
	 */
	public Long getKkm() {
		return kkm;
	}

	/**
	 * @param kkm
	 *            the kkm to set
	 */
	public void setKkm(Long kkm) {
		this.kkm = kkm;
	}

	/**
	 * @return the isRandomQuestion
	 */
	public Boolean getIsRandomQuestion() {
		return isRandomQuestion;
	}

	/**
	 * @param isRandomQuestion
	 *            the isRandomQuestion to set
	 */
	public void setIsRandomQuestion(Boolean isRandomQuestion) {
		this.isRandomQuestion = isRandomQuestion;
	}

	/**
	 * @return the isRandomAnswer
	 */
	public Boolean getIsRandomAnswer() {
		return isRandomAnswer;
	}

	/**
	 * @param isRandomAnswer
	 *            the isRandomAnswer to set
	 */
	public void setIsRandomAnswer(Boolean isRandomAnswer) {
		this.isRandomAnswer = isRandomAnswer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((duration == null) ? 0 : duration.hashCode());
		result = prime * result + ((isRandomAnswer == null) ? 0 : isRandomAnswer.hashCode());
		result = prime * result + ((isRandomQuestion == null) ? 0 : isRandomQuestion.hashCode());
		result = prime * result + ((kkm == null) ? 0 : kkm.hashCode());
		result = prime * result + ((questionCode == null) ? 0 : questionCode.hashCode());
		result = prime * result + ((questionTitle == null) ? 0 : questionTitle.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof QuestionHeader))
			return false;
		QuestionHeader other = (QuestionHeader) obj;
		if (duration == null) {
			if (other.duration != null)
				return false;
		} else if (!duration.equals(other.duration))
			return false;
		if (isRandomAnswer == null) {
			if (other.isRandomAnswer != null)
				return false;
		} else if (!isRandomAnswer.equals(other.isRandomAnswer))
			return false;
		if (isRandomQuestion == null) {
			if (other.isRandomQuestion != null)
				return false;
		} else if (!isRandomQuestion.equals(other.isRandomQuestion))
			return false;
		if (kkm == null) {
			if (other.kkm != null)
				return false;
		} else if (!kkm.equals(other.kkm))
			return false;
		if (questionCode == null) {
			if (other.questionCode != null)
				return false;
		} else if (!questionCode.equals(other.questionCode))
			return false;
		if (questionTitle == null) {
			if (other.questionTitle != null)
				return false;
		} else if (!questionTitle.equals(other.questionTitle))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QuestionHeader [questionCode=" + questionCode + ", questionTitle=" + questionTitle + ", duration="
				+ duration + ", kkm=" + kkm + ", isRandomQuestion=" + isRandomQuestion + ", isRandomAnswer="
				+ isRandomAnswer + "]";
	}

}
