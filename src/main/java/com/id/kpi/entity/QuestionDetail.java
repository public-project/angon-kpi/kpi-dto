/**
 * 
 */
package com.id.kpi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.id.kpi.base.BaseEntity;
import com.id.kpi.enums.QuestionDetailEnum;

/**
 * @author aulia
 *
 */
@Entity
@Table
public class QuestionDetail extends BaseEntity {

	private static final long serialVersionUID = 1826875851651643419L;

	@Column
	private String questionDescription;

	@Column
	private String answerA;

	@Column
	private String answerB;

	@Column
	private String answerC;

	@Column
	private String answerD;

	@Column
	private String answerE;

	@Column
	private QuestionDetailEnum model;

	@Column
	private String answerKey;

	@ManyToOne
	@JoinColumn(name = "header_id", nullable = false)
	private QuestionHeader questionHeader;

	/**
	 * Default constructor
	 */
	public QuestionDetail() {
		super();
	}

	/**
	 * @param questionDescription
	 * @param answerA
	 * @param answerB
	 * @param answerC
	 * @param answerD
	 * @param answerE
	 * @param model
	 * @param answerKey
	 * @param questionHeader
	 */
	public QuestionDetail(String questionDescription, String answerA, String answerB, String answerC, String answerD,
			String answerE, QuestionDetailEnum model, String answerKey, QuestionHeader questionHeader) {
		this.questionDescription = questionDescription;
		this.answerA = answerA;
		this.answerB = answerB;
		this.answerC = answerC;
		this.answerD = answerD;
		this.answerE = answerE;
		this.model = model;
		this.answerKey = answerKey;
		this.questionHeader = questionHeader;
	}

	/**
	 * @return the questionDescription
	 */
	public String getQuestionDescription() {
		return questionDescription;
	}

	/**
	 * @param questionDescription
	 *            the questionDescription to set
	 */
	public void setQuestionDescription(String questionDescription) {
		this.questionDescription = questionDescription;
	}

	/**
	 * @return the answerA
	 */
	public String getAnswerA() {
		return answerA;
	}

	/**
	 * @param answerA
	 *            the answerA to set
	 */
	public void setAnswerA(String answerA) {
		this.answerA = answerA;
	}

	/**
	 * @return the answerB
	 */
	public String getAnswerB() {
		return answerB;
	}

	/**
	 * @param answerB
	 *            the answerB to set
	 */
	public void setAnswerB(String answerB) {
		this.answerB = answerB;
	}

	/**
	 * @return the answerC
	 */
	public String getAnswerC() {
		return answerC;
	}

	/**
	 * @param answerC
	 *            the answerC to set
	 */
	public void setAnswerC(String answerC) {
		this.answerC = answerC;
	}

	/**
	 * @return the answerD
	 */
	public String getAnswerD() {
		return answerD;
	}

	/**
	 * @param answerD
	 *            the answerD to set
	 */
	public void setAnswerD(String answerD) {
		this.answerD = answerD;
	}

	/**
	 * @return the answerE
	 */
	public String getAnswerE() {
		return answerE;
	}

	/**
	 * @param answerE
	 *            the answerE to set
	 */
	public void setAnswerE(String answerE) {
		this.answerE = answerE;
	}

	/**
	 * @return the model
	 */
	public QuestionDetailEnum getModel() {
		return model;
	}

	/**
	 * @param model
	 *            the model to set
	 */
	public void setModel(QuestionDetailEnum model) {
		this.model = model;
	}

	/**
	 * @return the answerKey
	 */
	public String getAnswerKey() {
		return answerKey;
	}

	/**
	 * @param answerKey
	 *            the answerKey to set
	 */
	public void setAnswerKey(String answerKey) {
		this.answerKey = answerKey;
	}

	/**
	 * @return the questionHeader
	 */
	public QuestionHeader getQuestionHeader() {
		return questionHeader;
	}

	/**
	 * @param questionHeader
	 *            the questionHeader to set
	 */
	public void setQuestionHeader(QuestionHeader questionHeader) {
		this.questionHeader = questionHeader;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((answerA == null) ? 0 : answerA.hashCode());
		result = prime * result + ((answerB == null) ? 0 : answerB.hashCode());
		result = prime * result + ((answerC == null) ? 0 : answerC.hashCode());
		result = prime * result + ((answerD == null) ? 0 : answerD.hashCode());
		result = prime * result + ((answerE == null) ? 0 : answerE.hashCode());
		result = prime * result + ((answerKey == null) ? 0 : answerKey.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((questionDescription == null) ? 0 : questionDescription.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof QuestionDetail))
			return false;
		QuestionDetail other = (QuestionDetail) obj;
		if (answerA == null) {
			if (other.answerA != null)
				return false;
		} else if (!answerA.equals(other.answerA))
			return false;
		if (answerB == null) {
			if (other.answerB != null)
				return false;
		} else if (!answerB.equals(other.answerB))
			return false;
		if (answerC == null) {
			if (other.answerC != null)
				return false;
		} else if (!answerC.equals(other.answerC))
			return false;
		if (answerD == null) {
			if (other.answerD != null)
				return false;
		} else if (!answerD.equals(other.answerD))
			return false;
		if (answerE == null) {
			if (other.answerE != null)
				return false;
		} else if (!answerE.equals(other.answerE))
			return false;
		if (answerKey == null) {
			if (other.answerKey != null)
				return false;
		} else if (!answerKey.equals(other.answerKey))
			return false;
		if (model != other.model)
			return false;
		if (questionDescription == null) {
			if (other.questionDescription != null)
				return false;
		} else if (!questionDescription.equals(other.questionDescription))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QuestionDetail [questionDescription=" + questionDescription + ", answerA=" + answerA + ", answerB="
				+ answerB + ", answerC=" + answerC + ", answerD=" + answerD + ", answerE=" + answerE + ", model="
				+ model + ", answerKey=" + answerKey + "]";
	}

}
